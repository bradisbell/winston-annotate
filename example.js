var logger = require('winston');

logger.info('test');

var loggerA = require('./lib/annotate.js')(logger, '[prefix A]');
loggerA.info('test');

loggerA = require('./lib/annotate.js')(loggerA, '[prefix A2]');
loggerA.warn('test', {data: 'some data!'});

var loggerB = require('./lib/annotate.js')(logger, '[prefix B]');

loggerB.info('testB');
loggerA.info('testA');