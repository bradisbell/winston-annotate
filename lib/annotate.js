var _ = require('underscore');

module.exports = function annotate(logger, prefix) {
  var self = this;
  var newLogger = {};
  
  prefix = prefix || '';
  
  _.each(_.functions(logger), function (funcName) {
    newLogger[funcName] = function () {
      arguments[0] = prefix + ' ' + arguments[0];
      logger[funcName].apply(logger, arguments);
    }
  });
  
  return newLogger;
}